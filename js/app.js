window.onload = function(){
	init();
}

var canvas, select, context, stage, i=0, t_evento=0, it=0, dfa=false, pdfa, flag = false, valCable, contMetraje=0, contMufasa=0;
var v_estados = [], v_cable = [], v_transicion = [], alfabeto = [], palabra = [], estados = [], ax, ay, estado, etrsc, aetrsc, trans;

function init(){
	var contenedor = document.getElementById("contenedor");
	this.cargarDatos();

	if(Modernizr.canvas){/*comprueba si el contenedor es aceptado por el motor de busqueda*/
		canvas = document.createElement("canvas");
		canvas.width = 600;
		canvas.height = 600;
		contenedor.appendChild(canvas);/* agrega el canvas al contenedor del html*/

		context = canvas.getContext("2d");

		//stage = new createjs.Stage(canvas);
		stage = new createjs.Stage(canvas);/*libreria escenario donde se trabaja*/


		//alfa(prompt('Ingrese el alfabeto separado por espacio',''));/*metodo para leer alfabeto*/

		//canvas.save();
		

		stage.addEventListener("stagemousedown", mouseDown);/*evento para leer del mause en el escenario*/
		this.tpoCable();
		

	}else{
		contenedor.innerHTML = "El navegador no soporta canvas";/*en caso de que el navegador no acepte el canvas*/
	}

}

function cargarDatos(){
	v_cable.push({
		metro: 60,
		mufasa: 0
	});
	v_cable.push({
		metro: 300,
		mufasa: 2
	});
	v_cable.push({
		metro: 0,
		mufasa: 0
	});
	v_cable.push({
		metro: 235,
		mufasa: 4
	});
	v_cable.push({
		metro: 235,
		mufasa: 0
	});
	v_cable.push({
		metro: 300,
		mufasa: 2
	});
}

function tpoCable() {
	select = document.getElementById('tipoCable');
		select.addEventListener('change',
			function(){
				valCable = this.options[select.selectedIndex].text;
				//alert(valCable);
			});
	return valCable;
}

function resetAll() {/*resetear todo*/    /*Moooodiiifiqueeeeee*/
	location.href="cableado.php";
	canvas = null; 
	context = null; 
	stage = null; 
	i=0;
	t_evento=0;
	it=0;
	dfa=false;
	pdfa = null; 
	v_estados = null; 
	v_transicion = null; 
	alfabeto = null; 
	palabra = null; 
	estados = null; 
	ax = null; 
	ay = null; 
	estado = null; 
	etrsc = null; 
	aetrsc = null; 
	trans = null; /**/
	//canvas.restore();
}


function alfa(text){/*separar el alfabeto*/
	alfabeto = text.split(" ");

	/*for (i = 0; i < alfabeto.length; i++) {
		alert(alfabeto[i]);
	}*/
}

function tpEvento(num){
	var _self = this;/*todo el contenido del java script*/
	
	t_evento = num;
	if(t_evento == 0){
		alert("EScoge una opción");  /* creo estado inicial*/		
	}
	if(t_evento == 1){
		stage.addEventListener("mousedown",mouseDown);  /* creo estado inicial*/		
	}
	if(t_evento == 4) {
		_self.probar();		
	}
	if(t_evento == 5) {
		_self.verificarTransiciones();
		if(pdfa == true)
			_self.probarPalabra(prompt('Ingrese la Palabra',''));		
	}
	if(t_evento == 6){
		_self.resetAll();		
	}
	if(t_evento == 3){
		alert("Metraje: " + contMetraje);
		/*alert("Entra");	transiciones*/	
	}
}
function mouseDown(e) {
	var _self = this;
	
	if(t_evento == 1){
		_self.crearEstadoA(e);  /* creo estado inicial*/		
	}
	/*if(t_evento == 1 || t_evento == 2 || t_evento == 7){ Modiifiqueeeeeeeeeeeeeeeeeeeeeeee
		_self.crearEstado(e);		/* estado normal y final*/
	//}*/
	if(t_evento == 2){
		stage.addEventListener("mousedown",crearTransicion);
		/*alert("Entra");	transiciones*/	
	}

	stage.update();/*actualizar el canvas*/
}

function crearEstadoA(e){
		estado = new Image();
		estado.src = "img/computador.png";
		var bitmap = new createjs.Bitmap(estado);
		bitmap.x = e.stageX - 20;
		bitmap.y = e.stageY - 45;
		if(bitmap.x<200)
			bitmap.x = 0;
		else{
			if (bitmap.x<400 && bitmap.x >=200)
				bitmap.x = 200;
			else{
				if (bitmap.x<600 && bitmap.x >=400)
					bitmap.x = 400;
				else{
					if (bitmap.x<800 && bitmap.x >=600)
						bitmap.x = 600;
				}
			}
		}
		if(bitmap.y<200)
			bitmap.y = 0;
		else{
			if (bitmap.y<400 && bitmap.y >=200)
				bitmap.y = 200;
			else{
				if (bitmap.y<600 && bitmap.y >=400)
					bitmap.y = 400;
				else{
					if (bitmap.y<800 && bitmap.y >=600)
						bitmap.y = 600;
				}
			}
		}
		bitmap.scaleX = 0.2;
		bitmap.scaleY = 0.2;

		nv=true;

		for(i = 0; i< v_estados.length; i++){
			if(v_estados[i].x == bitmap.x && v_estados[i].y == bitmap.y){
				alert("Esa estación ya está");
				nv=false;
			}
		}


		if(nv){
			stage.addChild(bitmap);
			stage.update();
			v_estados.push({ /*vector de los estados*/
		 		x: bitmap.x,
		 		y: bitmap.y
		 	});
		}
	
}
//var band = 0;
/*var select = document.getElementById('tipoCable');
	select.addEventListener('change',
		function(){
			var valCable = this.options[select.selectedIndex];
			alert(valCable);
		});*/

function radianes(grados){
		 var radianes = (Math.PI/180)*grados;
		 return radianes;
}

function crearTransicion(e){
	trans = new createjs.Shape();
	var _self = this;

	

	band = 1;

	for(i=0; i< v_transicion.length ; i++){
		if(v_transicion[i].valor == valCable){
			band = 0;
		}
	}

	if(band==1){
		if(ax != null && ay != null){
			trans.graphics.setStrokeStyle(3);
			trans.graphics.beginStroke('black');
			trans.graphics.moveTo(ax,ay);/*punto inicial*/

			trans.graphics.lineTo(e.stageX,e.stageY);/*punto final*/
			trans.graphics.draw(context);
			stage.addChild(trans);/*dibajar en lienzo*/
			//band = 0;
			
			
			
			//do{
				//var nv=true;
				

				/*for (i = 0; i < v_transicion.length; i++) { /*Mooodifiqueeeeeeeeeeeeeeeeeeeee*/
				/*	if(v_transicion[i].einicial == aetrsc && v_transicion[i].valor == val){
						nv = true;
						alert("EPA: Este valor para la transicion de estos estados ya EXISTE");
						ax = null; 
						ay = null; 
						etrsc = null;
						break;
					}
				}

			//}while(nv==true);

			/*	if(!(estado.valor == undefined || estado.valor == "")){
					stage.addChild(trans);/*dibajar en lienzo*/
					/*Pintar valor de la transicion*/
			/*		if(band == 0){
						var text = new createjs.Text(val, "16px Arial", "#fff");
						text.x = (ax+e.stageX)/2;
						text.y = (ay+e.stageY+10)/2;
						stage.addChild(text);
					}
					else{
						var text = new createjs.Text(val, "16px Arial", "#fff");
						text.x = ((ax+e.stageX)/2)+9;
						text.y = ((ay+e.stageY+10)/2)-70;
						stage.addChild(text);
					}
					it++;
				} */
			
			
			
			stage.update();

			ax=ay=null;
			v_transicion.push({/*vector de transiciones*/
				valor: valCable
			});

			
			alert(this.valCable);
			contMetraje = contMetraje + v_cable[v_transicion[v_transicion.length-1].valor].metro;
			contMufasa = contMufasa + v_cable[v_transicion[v_transicion.length-1].valor].mufasa;
		
		}else{
		
			ax = e.stageX;/*asigna el valor de los puntos iniciales si no los marco */
			ay = e.stageY;
			//aetrsc = etrsc;
			//alert(etrsc);
			//_self.probTran1(etrsc);
		}	
	}else{
		alert("Ese cable ya se utilizó en otra estación");
	}
}
function probarPalabra(text){
    var _self = this;
    palabra = text.split("");
    _self.automata();
}

function automata(){/*verificar el estado inicial*/
    var k=0, nv, ea, et;
    
    for(i= 0; i < v_estados.length; i++){
        if(v_estados[i].tipo == 0 || v_estados[i].tipo == 7){ /*Modifiqueeeeeeeeeeeeeeeeeeeeeeeeeee*/
            ea = v_estados[i].valor;
            estado.graphics.beginFill('purple').drawCircle(v_estados[i].x, v_estados[i].y, 30);
            break;
        }
    }
    
    do{
    	nv = true;
		
		for(var j = 0; j < v_transicion.length; j++){
			if(v_transicion[j].einicial == ea && v_transicion[j].valor == palabra[k]){
				nv = false;
				for(i = 0; i < v_estados.lengh; i++){
					if(v_estados[i].valor == ea && v_estados[i] == 0){
						estado.graphics.beginFill('red').drawCircle(v_estados[i].x, v_estados[i].y, 30);
					}
					if(v_estados[i].valor == ea && v_estados[i] == 1){
						estado.graphics.beginFill('blue').drawCircle(v_estados[i].x, v_estados[i].y, 30);
					}
					if(v_estados[i].valor == ea && v_estados[i] == 2){
						estado.graphics.beginFill('yellow').drawCircle(v_estados[i].x, v_estados[i].y, 30);
					}
				}
				
				ea = v_transicion[j].efinal;
				
				/*Para pintar la ruta*/
				
				for(i = 0; i < v_estados.length; i++){
					if(v_estados[i].valor == ea){
						estado.graphics.beginFill('purple').drawCircle(v_estados[i].x, v_estados[i].y, 30);
						et = v_estados[i].tipo;
						stage.update();
					}
				}
				
				k++;
				break;
			}
		}
    }while(nv == false); /*Comprobar que no existen mas letras a evaluar*/
	
	if(k == palabra.length && (et == 2 || et == 7)) /*Comprobar que la palabra sea aceptada*/  /*Modifiqueeeeeeeeeeeeeeeeeeeeeeeeeee*/
		alert("Palabra Aceptada");
	else
		alert("No aceptada");
}

function probar(){
	var ini = prompt('Ingrese Estado Inicial', '');
	var fin = prompt('Ingrese Estado Final', '');
	
	for(var k = 0; k < v_transicion.length; k++){
		if(v_transicion[k].einicial == ini && v_transicion[k].efinal == fin){
			alert(v_transicion[k].valor);
		}
	}
}

function verificarTransiciones(){
	var k = 0;
	
	pdfa = false;
	
	for(i = 0; i < v_estados.length; i++){
		for(var j = 0; j < v_transicion.length; j++){
			if(v_estados[i].valor == v_transicion[j].einicial){
				k++;
			}
		}
		
		if(k < alfabeto.length){
			alert("EPA: No estas declarando todas las transiciones del alfabeto en un estado!!");
			pdfa = false;
			break;
		}else{
			pdfa = true;
		}
		
		k = 0;
	}
}

function probTran1(est_tran, val){
	var con = 0;

	for (i = 0; i < v_transicion.length; i++) {
		if(v_transicion[i].einicial == est_tran)
			con++;
	}

	if(con == alfabeto.length){
		alert("EPA: Este estado ya posee todas sus transiciones");
		ax = null; 
		ay = null; 
		etrsc = null;
	}

}